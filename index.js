// Part 1: Query Operators and Projections


// Copy the Sample database:
// Create a database in Robo 3T with a DB name: reading-act.
// Open a shell and execute your codes one at a time.
// Save a screenshot for every solution

db.booksale.insertMany([
    {
        "name": "A Rosy Life",
        "author": "Sam Petterson",
        "price": 3500,
        "stocks": 80,
        "publisher": "Apple Publishing House"
    },
    {
        "name": "Tomorrow Starts Today",
        "author": "Johnny Mayer",
        "price": 2500,
        "stocks": 10,
        "publisher": "Apple Publishing House"
    },
    {
        "name": "Welcome to Skypeia",
        "author": "Carl Watson",
        "price": 6000,
        "stocks": 78,
        "publisher": "Big Idea Publishing"
    },
    {
        "name": "Secret to Good Parenting",
        "author": "Martin Reyes",
        "price": 10000,
        "stocks": 112,
        "publisher": "Lighthouse Publishing "
    },
    {
        "name": "Daily Dose of Magic",
        "author": "Martha Williams",
        "price": 4500,
        "stocks": 32,
        "publisher": "Lighthouse Publishing "
    }
]);


/*
1. In the booksale collection, return all the documents that have the price equal or greater than 4000. (include a screenshot in your solution)

*/
// Code here:

    db.booksale.find(
        {"price": {$gte : 4000}}
    )


/*
2. In the booksale collection, return all the documents that have stocks of greater than 50 and less than 100. (include a screenshot in your solution)
*/
// Code here:

    db.booksale.find(
        {"stocks": {$gt : 50} && {$lt : 100}}
    )

/*
3. In the booksale collection, find the author/s that has 'mar' in their name or publisher that has 'apple' in its name. (include a screenshot in your solution)

*/
// Code here:

    db.booksale.find({
        $or: [
            {"author": {$regex : 'mar',$options: '$i'}},
            {"publisher": {$regex : 'apple', $options : '$i'}}
        ]
    })


/*
4. In the booksale collection, update the publisher to "Big Idea Publishing" if the author's name has 'sam'. (include a screenshot in your solution)

*/
// Code here:

    db.booksale.updateMany(
        {"author": {$regex : 'sam', $options : '$i'}},
        {
            $set : {
                "publisher": "Big Idea Publishing"
            }
        }
    )



/*
5. In the booksale collection, update the price of several books that has stocks greater than or equal 60. The new price should be 4999. (include a screenshot in your solution)

*/
// code here:

db.booksale.updateMany(
    {"stocks": {$gte : 60}},
    {
        $set : {
            "price": 4999
        }
    }
)