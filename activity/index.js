// Part 2: Simple Server - Node.js

/*
	1. Create an index.js file inside of the activity folder. 
	2.Import the http module using the required directive. Create a variable port and assign it with the value of 8000.
	3. Create a server using the createServer method that will listen in to the port provided above.
	4. Console log in the terminal a message when the server is successfully running.
	5. Create a condition that when the login route, the register route, the homepage and the usersProfile route is accessed, it will print a message specific to the page accessed.
	6. Access each routes to test if it’s working as intended. Save a screenshot for each test.
	7. Create a condition for any other routes that will return an error message.

*/
// Code here:



const http = require('http');

const port = 8000;

const server = http.createServer(function(req,res){

	let url = req.url;
	let page = url.replace('/','');
	let pages = ['login','homepage','register','usersprofile'];
	let isDefinedPage = pages.includes(page);

	if(isDefinedPage){
		res.writeHead(200,{'Content-Type':'text/html'})
		res.end(`<h1>You are on the ${page.toUpperCase()} page</h1>`);
	}else{
		res.writeHead(404,{'Content-Type':'text/html'})
		res.end(`<h1>Error 404: Page Not Found</h1>`);
	}

})

server.listen(port);

console.log(`Server is running on localhost:${port}`);

